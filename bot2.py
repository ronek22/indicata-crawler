from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from models.Car import Car
from models.Body import Body
from time import sleep
from selenium.common import exceptions
from retry import retry
import http.client
import json
import os
import traceback


class Bot:
    def __init__(self):
        options = webdriver.FirefoxOptions()
        # options.add_argument("--headless")
        profile = webdriver.FirefoxProfile()
        profile.DEFAULT_PREFERENCES['frozen']["dom.webdriver.enabled"] = False
        self.driver = webdriver.Firefox(options=options,firefox_profile=profile, executable_path=GeckoDriverManager().install())
        self.wait = WebDriverWait(self.driver, 60)
        self.quickWait = WebDriverWait(self.driver, 10)
        self.driver.get(
            "https://lg.indicata.com/#/?dealerId=4793088a-4328-4ac7-a4c8-587fc3e087dd")

        self.url_map = {
            "make": self.get_buttons,
            "model": self.get_buttons,
            "regmonth": self.chooseYear,
            "seats": self.get_buttons,
            "engine": self.get_buttons,
            "body": self.chooseBody,
            "facelift": self.chooseFacelift,
            "wheeldrive": self.get_buttons,
            "transmission": self.get_buttons,
            "trim": self.chooseEdition,
            "contact": self.chooseContact,
            "bodyLength": self.get_buttons,
            "bodyHeight": self.get_buttons,
            "weight": self.get_buttons,
            "category": self.wait_and_refresh,
        }
        #Add bodyLength
        self.path_map = {
            "make": 0,
            "model": 1,
            "regmonth": 2,
            "body": 3,  # aka karosserie
            "facelift": 4,
            "seats": 5,
            "bodyHeight":6,
            "bodyLength":7,
            "weight":8,
            "engine": 9,
            "wheeldrive": 10,
            "transmission": 11,
            "trim": 12,
        }

        self.name = ''
        self.inverted_path_map = {v: k for k, v in self.path_map.items()}
        self.cars = []
        self.path = [[], [], [], [], [], [], [], [], [], [], [], []]
        self.links = []
        self.finished = False
        self.current = ""
        self.editions = []
        self.latest_click = -1
        self.added = False
        self.seats = ['']
        self.bodyImages = {}
        self.liftImages = {}

    def wait_and_refresh(self):
        print("CRAWLER DETECED WAIT 600sec")
        sleep(600)
        self.return_after_exception()

    def return_after_exception(self):
        if self.finished:
            return "DONE FROM EXCEPTION"
        sleep(120)
        self.driver.refresh() # refresh to get new session
        sleep(20)
        # TYP SAMOCHODOW
        self.click_button('Personenkraftwagen')
        self.click_button(self.currentCar.marke)
        # MARKA
        if self.check_url() == 'model':self.click_button(self.currentCar.modell)
        # ROCZNIK
        actual_year = str(int(self.path[2][0])+1)
        self.click_button(actual_year)
        while(self.click_last_value() != "TIME TO RUN"): print("BACKUP BY CLICKING")
        self.run_for_model(self.name)



    def click_button(self, text):
        current = self.driver.current_url
        button = self.wait.until(ec.presence_of_element_located((By.XPATH, f"//*[contains(text(), '{text}')]")))
        button.click()
        self.quickWait.until(ec.url_changes(current))


    def click_last_value(self):
        buttons = self.get_buttons()
        current = self.check_url()

        if current == 'engine' and not self.path[10] and not self.path[11]:
            return "TIME TO RUN"

        if current in ["wheeldrive", "transmission", "trim"]:
            return "TIME TO RUN"

        followedValues = self.path[self.path_map[current]]
        indexOfClickedButton = 0
        if not followedValues:
            indexOfClickedButton = len(buttons)-1
        else:
            indexOfClickedButton = len(buttons) - len(followedValues) - 1
        buttons[indexOfClickedButton].click()
        self.quickWait.until(ec.url_changes(current))
        return "DONE"


    def run_for_model(self, modelname):
        try:
            if not self.name:
                self.name = modelname
            self.finished = False
            while not self.finished:
                self.update()
            print("DONE")
            self.save(modelname)
            self.send(f"Finished: {modelname}")
            os.system('play -nq -t alsa synth {} sine {}'.format(1, 440))
            self.reset()
        except KeyboardInterrupt:
            print('Interrupt Bot')

    def save(self, modelname):
        with open(f"nutz/{modelname}.json", 'w', encoding='utf-8') as f:
            json.dump(self.cars, f, indent=4,
                      default=lambda x: x.__dict__, ensure_ascii=True)

    @retry((exceptions.StaleElementReferenceException, IndexError), tries=3, delay=1)
    def update(self):
        try:
            buttons = self.runMapped()
            if self.finished: return "ENDED"
            if self.added:
                self.added = False
                return "ADDED"
            self.clear_body()
            index = self.index()
            if self.latest_click < index: self.path[index] = [x.text for x in buttons]
            else:
                if self.path[index]: buttons = [x for x in buttons if x.text in self.path[self.index()]]
                else: return self.goUp(buttons)  
            if self.current not in ["trim", "wheeldrive", "transmission"]:
                self.path[self.index()] = self.path[self.index()][1:]
            # print(f"CLICKED BUTTON: {buttons[0].text}")
            current = self.driver.current_url
            # print(f"CURRENT: {current}")
            self.latest_click = index
            if self.current == "facelift": self.clicked_lift = buttons[0].text
            if self.current == "body": self.clicked_body = buttons[0].text
            if self.current == "seats":
                self.seats = [x.text for x in buttons]
                self.path[index] = []
            buttons[0].click()
            self.quickWait.until(ec.url_changes(current))
        except exceptions.TimeoutException as e:
            print("TIMEOUT\tCATPCHA\tREFRESH")
            self.send(f"Timeout Exception\n{self.path[1]}\n{self.path[2]}")
            self.return_after_exception()
            os.system('play -nq -t alsa synrth {} sine {}'.format(1, 440))
            raise
        except KeyError as e:
            print(f"Key Error: {traceback.print_exc()}")
            self.send(f"Key Error: {e}")
            raise

    def clear_body(self):
        if self.index() < 3:
            self.bodyImages = {}
            self.clicked_body = ''

        if self.index() < 4:
            self.liftImages = {}
            self.clicked_lift = ''

        if self.index() < 5:
            self.seats = ['']


    def isEnded(self):
        self.current = self.check_url()
        if not any(self.path[1:]) and self.cars and (self.current == "trim" or self.current == "contact"):
            # self.saveData()
            self.finished = True
            return True
        return False

    def go_to_test_model(self):
        self.update_and_click_button()
        self.update_and_click_button()
        self.update_and_click_button()
        # Now in 124 model
        print("PATH: ", self.path)

    # region chooseMethods

    # TODO: STALE EXCEPTION TODO HERE
    def get_buttons(self):
        if self.current in ['bodyLength', 'bodyHeight']:
            links = self.wait.until(ec.visibility_of_all_elements_located((By.CLASS_NAME, 'nav-link')))
            if self.current == 'bodyHeight':
                links[7].click()
            elif self.current=='bodyLength':
                links[8].click()
            sleep(0.5)

        return self.wait.until(ec.visibility_of_all_elements_located((By.CLASS_NAME, 'mt-1')))

    def chooseYear(self):
        self.wait_and_set_content('year')
        return self.content.find_elements_by_tag_name('button')

    def chooseEdition(self):
        if self.isEnded():
            return "ENDED FROM EDITION"
        self.saveData()
        self.goUp()
        self.added = True
        # print("FINISH ENDED")

    def chooseContact(self):
        if self.isEnded():
            return "ENDED FROM CONTACT"
        self.saveData()
        self.goUp()
        self.added = True

    def chooseBody(self):
        buttons = self.wait.until(
            ec.visibility_of_all_elements_located((By.CLASS_NAME, 'mt-1')))
        if not self.bodyImages:
            for button in buttons:
                images = button.find_elements_by_tag_name('img')
                img_list = [image.get_attribute("src") for image in images]
                self.bodyImages[button.text] = img_list
        return buttons

    def chooseFacelift(self):
        buttons = self.wait.until(
            ec.visibility_of_all_elements_located((By.CLASS_NAME, 'mt-1')))
        if not self.liftImages:
            for button in buttons:
                images = button.find_elements_by_tag_name('img')
                img_list = [image.get_attribute("src") for image in images]
                self.liftImages[button.text] = img_list
        return buttons

    def saveData(self):
        editions = ['']
        if self.current == "trim":
            editions = self.wait.until(
                ec.visibility_of_all_elements_located((By.CLASS_NAME, 'mt-1')))
            editions = [x.text for x in editions]
        data = self.summary()
        body = None
        lift = None
        if not self.bodyImages:
            body = Body(data['Karosserie'], [])
        else:
            body = Body(self.clicked_body, self.bodyImages[self.clicked_body])
        if not self.liftImages:
            lift = Body(data['Facelift'], [])
        else:
            lift = Body(self.clicked_lift, self.liftImages[self.clicked_lift])
        transmissions = self.path[self.path_map['transmission']]
        wheeldrives = self.path[self.path_map['wheeldrive']]
        if not transmissions:
            transmissions = ['']
        if not wheeldrives:
            wheeldrives = ['']
        self.currentCar = Car(data['Marke'], data['Modell'], data['Erstzulassung'], 
        body.name, lift.name, data['Sitze'], data['Motor'], data['Antrieb'], 
        data['Getriebe'], 'edition', data['Höhe'],data['Länge'],data['Gewicht'])
        for seat in self.seats:
            for wheeldrive in wheeldrives:
                for transmission in transmissions:
                    for edition in editions:
                        self.cars.append(Car(
                            marke=data['Marke'],
                            modell=data['Modell'],
                            erstzulassung=data['Erstzulassung'],
                            karosserie=body,
                            facelift=lift,
                            sitze=data['Sitze'] if seat=='' else seat,
                            motor=data['Motor'],
                            antrieb=data['Antrieb'] if wheeldrive=='' else wheeldrive,
                            getriebe=data['Getriebe'] if transmission=='' else transmission,
                            ausstatung=edition,
                            hohe=data['Höhe'],
                            lange=data['Länge'],
                            gewicht=data['Gewicht']
                        ))
        self.path[self.path_map['transmission']] = []
        self.path[self.path_map['wheeldrive']] = []

    def reset(self):
        self.get_url("regmonth")
        self.latest_click = -1
        self.finished = False
        self.added = False
        self.editions = []
        self.cars = []
        self.seats = ['']
        self.path = [[], [], [], [], [], [], [], [], [], [], [], []]
        self.links = []
        self.current = ""
        self.name = ''

    # endregion

    # region helperMethods


  
    def send(self, message):
        webhookurl = "https://discordapp.com/api/webhooks/613400548120592398/tNUJUq-7ntfEEx2Ai6aEDWN-ahMsSD_qJCnapfUUXVBvxa2_0WdOQyJIp6SnYsjWY0Aa"
        formdata = "------:::BOUNDARY:::\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\n" + message + "\r\n------:::BOUNDARY:::--"
        connection = http.client.HTTPSConnection("discordapp.com")
        connection.request("POST", webhookurl, formdata, {
            'content-type': "multipart/form-data; boundary=----:::BOUNDARY:::",
            'cache-control': "no-cache",
            })
        response = connection.getresponse()
        result = response.read()
        return result.decode("utf-8")

    def wait_and_set_content(self, name):
        self.content = self.wait.until(
            ec.visibility_of_element_located((By.CLASS_NAME, name)))

    def goUp(self, buttons=[]):
        current = self.check_url()
        number = 11 if current == "contact" else self.path_map[self.current] - 1
        while not self.path[number]:
            number -= 1
        self.get_url(self.inverted_path_map[number])
        sleep(0.2)

    def printStack(self):
        toPrint = [f"{self.inverted_path_map[i]:12} | {v}" for i,
                   v in enumerate(self.path)]
        print(*toPrint, sep="\n")

    def isFinished(self):
        return check_url == "contact"

    def index(self):
        return self.path_map[self.current]

    def runMapped(self):
        """Update current url and run appropriate method that update self.content"""
        self.current = self.check_url()
        return self.url_map[self.current]()

    def summary(self):
        links = self.wait.until(
            ec.visibility_of_all_elements_located((By.CLASS_NAME, 'nav-link')))
        links = {x.text.split('\n')[0]:x.text.split('\n')[1] if len(x.text.split('\n'))==2 else '' for x in links}
        return links

    def check_url(self):
        return self.driver.current_url.split("/")[-1]

    def get_url(self, endpoint):
        url = f"https://lg.indicata.com/#/main/{endpoint}"
        self.driver.get(url)

    # endregion
