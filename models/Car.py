import json

class Car:
    def __init__(self, marke, modell, erstzulassung, karosserie, facelift, sitze, motor, antrieb, getriebe, ausstatung, hohe, lange, gewicht):
        self.marke = marke
        self.modell = modell
        self.erstzulassung = erstzulassung
        self.karosserie = karosserie
        self.facelift = facelift
        self.sitze = sitze
        self.motor = motor
        self.antrieb = antrieb
        self.getriebe = getriebe
        self.ausstatung = ausstatung
        self.hohe = hohe
        self.lange = lange
        self.gewicht = gewicht

    def __str__(self):
        return f"{self.marke}\t{self.modell}\t{self.erstzulassung}\n{self.karosserie}\t{self.facelift}\t{self.motor}\n"

    def __repr__(self):
        return f"{self.marke}-{self.modell}-{self.ausstatung}"

if __name__ == "__main__":
    car1 = Car('Honda', 'Civic', '2015-10', 'Coupe',
    'New Lights', '5', '1.8TDI', 'gre', 'sas', 'sas')
    car2 = Car('Suzuki', 'Vitara', '2012-10', 'Coupe',
    'New Lights', '5', '1.8TDI', 'gre', 'sas', 'sas')
    
    cars = [car1, car2]
    with open('cars.json', 'w') as f:
        json.dump(cars, f, default=lambda x: x.__dict__, ensure_ascii=True, indent=4)

