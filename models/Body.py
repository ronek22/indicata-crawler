import json

class Body:
    def __init__(self, name, images):
        self.name = name
        self.images = images

    def __str__(self):
        return f"{self.name}: {self.images}"